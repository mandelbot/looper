"""
    Looper
    Copyright (C) 2021  Mitchell Shipman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import subprocess
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio

class MyWindow(Gtk.Window):

	pactl_data = []
	grid = Gtk.Grid(column_spacing=5,row_spacing=5)

	def __init__(self):
		super().__init__(title="Looper")
		
		self.add(self.grid)
		
		self.refresh_source_list()
		
		hb = Gtk.HeaderBar()
		hb.set_show_close_button(True)
		hb.props.title = "Looper"
		self.set_titlebar(hb)
		
		button = Gtk.Button()
		icon = Gio.ThemedIcon(name="mail-send-receive-symbolic")
		image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
		button.add(image)
		button.connect("clicked", self.on_refresh_clicked)
		hb.pack_end(button)

	def destroy_grid_children(self, grid):
		for child in grid.get_children():
			child.destroy()

	def refresh_source_list(self):
		self.pactl_data = self.refresh_pactl_data()
		self.populate_grid(self.pactl_data)	

	def on_refresh_clicked(self, widget):
		self.refresh_source_list()
		
	#use pactl to return a list of sources and parse the data into lists	
	def refresh_pactl_data(self):
	
		#pactl list short sources returns a string with tab delimited data values and each row on a newline
		output = subprocess.run('pactl list short sources', shell=True, text=True,capture_output=True)
		
		#split the output into a list of each row
		rowlist = output.stdout.split("\n")  	 
		
		#loop through each row and split the columns into their own list
		#note that the [1] element of a split row contains a period delimited
		#set of values, split this and add it to the list to capture each
		#elemens individually   
		collist = []
		for i in range(0,len(rowlist)-1):
			collist.append(rowlist[i].split("\t") + rowlist[i].split("\t")[1].split(".")) 
			
		#print(collist)
		return collist

	#use pactl to return a list of active modules then compare to a provided source number
	#determine if a source already has the given module activated
	#module_string - a pactl module name to check (i.e. module-loopback)
	#source_string - a source index as defined by PulseAudio
	def find_pactl_module_ID(self, module_string, source_string):
	
		#get the list of modules and parse it into lists
		output = subprocess.run('pactl list short modules', shell=True, text=True,capture_output=True)
		rowlist = output.stdout.split("\n")  	    
		collist = []
		for i in range(0,len(rowlist)-1):
			collist.append(rowlist[i].split("\t"))
		
		#define the module_id then loop through the pactl output to determine
		#if the given source index has the given module activated already
		module_id = -1
		for i in range(0,len(collist)):
			if collist[i][1] == module_string:
				if collist[i][2] == source_string:
					module_id = collist[i][0]
		
		return module_id
		
	def populate_grid(self, source_list):
	
		self.destroy_grid_children(self.grid)
		self.remove(self.grid)
	
		#manually declare the table headers
		label = Gtk.Label(label="Source")
		self.grid.attach(label, 0,0,1,1)
		label = Gtk.Label(label="Name")
		self.grid.attach(label, 1,0,1,1)
		label = Gtk.Label(label="Type")
		self.grid.attach(label, 2,0,1,1)
		label = Gtk.Label(label="Card")
		self.grid.attach(label, 3,0,1,1)
		label = Gtk.Label(label="Status")
		self.grid.attach(label, 4,0,1,1)
		label = Gtk.Label(label="Monitor?")
		self.grid.attach(label, 5,0,1,1)
		label = Gtk.Label(label="Listen?")
		self.grid.attach(label, 6,0,1,1)
		
		#loop through list of sources and lay out the relevant data in a grid
		for i in range(0, len(source_list)):
			button = Gtk.Button(label=source_list[i][0])#Source
			self.grid.attach(button,0,(i*2)+1,1,1)
			button = Gtk.Button(label=source_list[i][6])#ShortName
			self.grid.attach(button,1,(i*2)+1,1,1)
			button = Gtk.Button(label=source_list[i][5])#Type
			self.grid.attach(button,2,(i*2)+1,1,1)
			button = Gtk.Button(label=source_list[i][2])#Card
			self.grid.attach(button,3,(i*2)+1,1,1)
			button = Gtk.Button(label=source_list[i][4])#Status
			self.grid.attach(button,4,(i*2)+1,1,1)
			
			#the last piece of the list will always be if the source is a monitor
			#compare to the string to set Y/N
			
			if source_list[i][len(source_list[i]) - 1] == "monitor":
				button = Gtk.Button(label="Y")
				self.grid.attach(button,5,(i*2)+1,1,1)
			else:
				button = Gtk.Button(label="N")
				self.grid.attach(button,5,(i*2)+1,1,1)
			
			#create the switch and check if the source is already set to loopback
			#so the button state is correct
			switch = Gtk.Switch()	
			module_id = self.find_pactl_module_ID('module-loopback', 'source=' + source_list[i][0])
			if module_id != -1:
				switch.set_active(True)
			switch.connect("notify::active", self.loopback, source_list[i][0])
			self.grid.attach(switch,6,(i*2)+1,1,1)
			
			#if this is not the last source, add a separator
			if i != (len(source_list) - 1):
				sep = Gtk.Separator(orientation="horizontal")
				self.grid.attach(sep, 0, (i*2)+2,7,1)
			
		self.add(self.grid)
		self.show_all()
	
	#debug function
	def on_button_clicked(self, widget):
		print(self.pactl_data)
		
		#run the pactl load-module command when a switch is toggled
	def run_load_module(self, source_num):
		subprocess.run('pactl load-module module-loopback source=' + source_num, shell=True, text=True,capture_output=True)
		
	#run the pactl unload-module command when a switch is toggled
	def run_unload_module(self, source_num):
		module_id = self.find_pactl_module_ID('module-loopback', 'source=' + source_num)
		subprocess.run('pactl unload-module ' + module_id, shell=True, text=True,capture_output=True)
			
	#for the passed data element, either enable or disable the loopback module
	#data[1] = pactl source index of the PulseAudio source the swich is asigned to
	def loopback(self, switch, *data):
		
		if switch.get_active():
			self.run_load_module(data[1])
		else:
			self.run_unload_module(data[1])
        


win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
